var Emission = require('../models/emissionModel');
var spline = require('cubic-spline');
const Helper = require('../helpers/helperFunctions');



/*
##########################################################################################################
   *                                                 
##########################################################################################################
*/
let interpolate = (l1, l2, d) => {
    for(var x = 0; x < l1.length; x++){
        if(d >= l1[x] && d < l1[x+1] && x < l1.length - 1){
            return spline(d,l1,l2)
        }
        if(d >= l1[l1.length-1]){
            let slope=Math.abs((l2[l2.length-1]-l2[l2.length-2])/(l1[l1.length-1]-l1[l1.length-2]));
            return l2[l2.length-1]+(slope*(d-l1[l1.length-1]))
        }
        if(d <= l1[0]){
            let slope=Math.abs((l2[1]-l2[0])/(l1[1]-l1[0]));
            return slope*d;
        }
    }
};
/*
##########################################################################################################
   *  A function to calculate the emissions of a component                                               
   *  Refer to the Emission schema for more information on the component                                                  
##########################################################################################################
*/

let find = (component, region, quantity) => {
    let emissions = {
        'CO2': 0,
        'CH4': 0,
        'N2O': 0
    }; // emissions accumulator
    return new Promise((resolve, reject) => {
        // find the component in the database
        if(quantity<0) reject('quantity cannot be negative');
        Emission.findOne({ 
            $or: [{ 
                'item': new RegExp(`^${component}$`, "i"), 
                'region': new RegExp(`^${region}$`, "i") 
            }, 
            // find the default values if a particular region is not found
            { 
                'item': new RegExp(`^${component}$`, "i"), 
                'region': 'Default' 
            }]
        }, (err, item) => {
            // if component is found
            if (!err && item) {
                console.log(`Item name: ${item.item} :: Region: ${item.region}`);
                // if component type is atomic return it's emissions
                if (item.components[0].name === 'CO2' ||
                    item.components[0].name === 'CH4' ||
                    item.components[0].name === 'N2O') {
                    for(let component of item.components){
                        if (emissions.hasOwnProperty(component.name)) {
                            emissions[component.name] += (quantity * component.quantity[0]);
                            console.log(`Emissions ${component.name}: ${emissions[component.name]} kg`);
                        }
                    }
                    resolve(emissions);
                }
                // if component type is complex, recurse to find its atomic components
                else {
                    let numOfComponents = item.components.length; // number of subcomponents
                    (async function(){
                        for (let i = 0; i < numOfComponents; i++) {
                            if(item.components[i].quantity.length > 1){
                                let getInterpolatedQuantity = await interpolate(item.quantity, item.components[i].quantity, quantity);
                                console.log(`Interpolated value = ${getInterpolatedQuantity}`);
                                await find(item.components[i].name, region, getInterpolatedQuantity)
                                        .then((emis) => {
                                            for(let i in emis){
                                                emissions[i] += emis[i];
                                            }
                                        })
                                        .catch((err) => console.log(err));
                            }
                            else {
                                await find(item.components[i].name, region, item.components[i].quantity[0])
                                        .then((emis) => {
                                            for(let i in emis){
                                                emissions[i] += emis[i];
                                            }
                                        })
                                        .catch((err) => console.log(err));
                            }
                        }
                    })().then(() => {
                        if(item.calculationMethod === 'interpolation'){
                            resolve(emissions);
                        }
                        else {
                            for(let i in emissions){
                                emissions[i] *= quantity;
                            }
                            resolve(emissions);
                        }
                    })
                    .catch((err) => console.log(err));
                }
            } 
            // return an error if component is not found
            else reject(`Unable to find component ${component} for ${region}`);
        });
    });
}


/*
##########################################################################################################
   * Calculates Emission for specified item                                                
##########################################################################################################
*/

exports.calculate = async function(itemName, region, quantity, multiply = 1){
    let emissions = await find(itemName, region, quantity);
    // round up the emission value upto 10 decimal points
    for(let i in emissions){
        emissions[i] = parseFloat((emissions[i]*multiply).toFixed(10));
        // remove CH4 or N2O key if emissions are zero
        if(!emissions[i] && i !== "CO2"){
            delete emissions[i];
        }
    }
    return emissions;
}

/*
##########################################################################################################
   * Calculates GHG emissions for things that can be directly related to emissions.
   * read the docs for detailed explaination for Supported Types and Payload                                      
##########################################################################################################
*/

exports.calculate_item_emission = function (req, res){
	let itemName = req.body["item"];
	let region = req.body["region"] || "Default";
	let quantity = req.body["quantity"] || 1;
    let multiply = req.body["multiply"] || 1;

	exports.calculate(itemName, region, quantity, multiply)
		.then((emissions) => {
			// console.log(`\nTotal Emissions: ${emissions.CO2}`);
			if (emissions.CO2 < 0) {
				res.status(200).json({
					success: true,
					emissions: emissions,
					unit: 'kg',
					note: "A negative number for emissions signifies that the item absorbs CO2."
				});
			} else {
				res.status(200).json({
					success: true,
					emissions: emissions,
					unit: 'kg'
				});
			}

		})
		.catch((err) => {
			console.log(`Error: ${err}`);
			res.status(400).json({
				success: false,
				err: err
			});
		});
}

/*
##########################################################################################################
   * Find Emissions for a flight between two airports.
   * Only ICAO Airport Codes are supported.
   * https://en.wikipedia.org/wiki/International_Civil_Aviation_Organization_airport_code                                                  
##########################################################################################################
*/
exports.calculate_flight_emission = function (req, res){
	let airports = require('../../../raw_data/airports.json');
	let type = req.body.type || 'international';
	let model = req.body.model;
	let origin = req.body.origin;
	let destination = req.body.destination;
	let passengers = req.body.passengers || 1;
	let seatType = req.body.seatType || 'economy';

	if (airports[origin] && airports[destination]) {
		let orig = airports[origin];
		let dest = airports[destination];
		let distance = Helper.getDistanceFromLatLon(orig.lat, orig.lon, dest.lat, dest.lon);
		distance *= 0.539957; // convert distance in km to nautical miles
		if (!model) {
			if (type == 'international') {
				model = 'A380';
			}
			if (type == 'domestic') {
				model = 'A320';
			}
		}

		exports.calculate(`airplane model ${model}`, 'Default', distance)
			.then((emissions) => {
				console.log(`\nTotal Emissions: ${emissions}`);
				res.status(200).json({
					success: true,
					emissions: emissions,
					unit: 'kg'
				});
			})
			.catch((err) => {
				console.log(`Error: ${err}`);
				res.status(404).json({
					success: false,
					err: `Unable to find emissions for airplane model ${model}`
				});
			});
	} else {
		res.status(400).json({
			success: false,
			error: 'Unable to find the airports. Please use ICAO airport codes only'
		});
	}

}

/*
##########################################################################################################
   * Calculate GHG emissions for a number of fuels.
   * The distance is calculated using Google Map Distant Matrix API.
   * The fuels that we currently support are listed in the docs.                                                  
##########################################################################################################
*/

exports.claculate_vechile_emission = async function(req, res)  {
	let type = req.body.type || 'Diesel';
	let origin = req.body.origin;
	let destination = req.body.destination;
	let unit = req.body.unit || 'km';
	let mileage = parseFloat(req.body.mileage) || 20;
	let mileage_unit = req.body.mileage_unit || 'km/l';

	if (origin && destination) {
		let distance = Helper.distance(origin, destination, 'driving');
		distance
			.then((val) => {
				console.log("CalculatedDistance= " + val);
				let fuelConsumed = val / mileage;
				console.log(fuelConsumed);
				exports.calculate(`fuel${type}`, 'Default', fuelConsumed)
					.then((emissions) => {
						console.log(`Emissions: ${JSON.stringify(emissions, null ,4)}`);
						res.status(200).json({
							success: true,
							emissions: emissions,
							unit: 'kg'
						});
					})
					.catch((err) => {
						console.log(`Error: ${err}`);
						res.status(404).json({
							success: false,
							err: `Unable to find emissions for fuel type ${type}`
						});
					});
			})
			.catch((err) => {
				console.log(`Error: ${err}`);
				res.status(400).json({
					success: false,
					err: err
				});
			});

	} else {
		res.status(400).json({
			success: false,
			error: 'Distance or Mileage cannot be less than zero'
		});
	}
}

/*
##########################################################################################################
   * Calculate GHG emissions for a number of train types.
   * The distance is calculated using Google Map Distant Matrix API.
   * The trains that we currently support are listed in the docs.                                                 
##########################################################################################################
*/
exports.calculate_train_emission =  async function(req, res) {
	let type = req.body.type || 'railcars';
	let region = req.body.region || 'Default';
	let origin = req.body.origin;
	let destination = req.body.destination;
	let passengers = req.body.passengers || 1;

	if (origin && destination) {
		let distance = Helper.distance(origin, destination, 'transit');
		distance
			.then((val) => {
				console.log("CalculatedDistance= " + val);
				console.log("CalculatedPassengers= " + passengers);
				exports.calculate(type, 'Default', val, passengers)
					.then((emissions) => {
						console.log(`Emissions: ${JSON.stringify(emissions, null ,4)}`);
						res.status(200).json({
							success: true,
							emissions: emissions,
							unit: 'kg'
						});
					})
					.catch((err) => {
						console.log(`Error: ${err}`);
						res.status(404).json({
							success: false,
							err: `Unable to find emissions for fuel type ${type}`
						});
					});
			})
			.catch((err) => {
				console.log(`Error: ${err}`);
				res.status(400).json({
					success: false,
					err: err
				});
			});

	} else {
		res.status(400).json({
			success: false,
			error: 'Distance cannot be less than zero'
		});
	}
}


/*
##########################################################################################################
   * Calculate GHG emissions for different type of Poutry meat production. 
   * In this we consider the factors like Production emission, Water loss factor, Moisture loss factor,
   * Post-farmgate emissions in different conditions according to region in which they are found. 
   * The different poultry type can be found in the docs                                                
##########################################################################################################
*/

exports.calculate_Poultry_emission =  async function(req, res) {
	let type = req.body.type;
	let region = req.body.region || 'Default';
	let quantity = req.body.quantity || 1;
	//console.log(`${type} in ${region} of mass ${quantity} kg`);
	if (type) {
		exports.calculate(type, region, quantity)
			.then((emissions) => {
				console.log(emissions);
				res.status(200).json({
					success: true,
					emissions: emissions,
					unit: 'kg'
				});
			}).catch((err) => {
				res.status(400).json({
					success: false,
					err: `We cannot provide carbon footprints for this combination of ${type} in ${region} of mass ${quantity} kg`
				})
			});
	} else {
		res.status(400).json({
			success: false,
			error: `Unable to find carbon footprint for type ${type}`
		});
	}
}

/*
##########################################################################################################
   * Calculate GHG emissions for a number of appliance types for a specific quantity and running time. 
   * The emissions are calculated from the electricity based emissions on a particular region
   * he different appliance types is referenced in the docs                                                
##########################################################################################################
*/

exports.calculate_appliances_emission = function(req, res) {
	let appliance = req.body["appliance"];
	let type = req.body["type"];
	let region = req.body["region"] || "Default";
	let unit = req.body["unit"] || "kWh";
	let quantity = req.body["quantity"] || 1;
	let runnning_time = req.body["runnning_time"] || 1;
	exports.calculate(`${appliance} ${type}`, region, quantity, runnning_time)
		.then((emissions) => {
			// console.log(`\nTotal Emissions: ${emissions.CO2}`);
			res.status(200).json({
				success: true,
				emissions: emissions,
				unit: 'kg'
			});
		})
		.catch((err) => {
			console.log(`Error: ${err}`);
			res.status(400).json({
				success: false,
				err: err
			});
		});
}

/*
##########################################################################################################
   * retrieve the quantity of a certain element provided the CO2 emission for the specific item is already known.
   * Refer to the GHG Emission doc for more details on items available.                                                
##########################################################################################################
*/

exports.calculate_item_quantity =  function(req, res) {
	let itemName = req.body["item"];
	let region = req.body["region"] || "Default";
    let emission = req.body["emission"] || 1;
	exports.calculate(itemName, region, 1, 1)
		.then((emissions) => {
			// console.log(`\nTotal Emissions: ${emissions.CO2}`);
			if(emissions.CO2){
				let quantity = Math.abs(emission/emissions.CO2);
				res.status(200).json({
					success: true,
					quantity: quantity,
					note: `This is a estimate for the quantity of ${itemName} that could be the cause of the emission provided.`
				});
			}
		})
		.catch((err) => {
			console.log(`Error: ${err}`);
			res.status(400).json({
				success: false,
				err: `Unable to find quantity for item type ${itemName}`
			});
		});
}