var express = require('express');
var router = express.Router();
// get the emission controller
const Emission = require('../controllers/emissionController');
/*
#######################################################################
|---------------------Emission Routes---------------------------------|
#######################################################################
*/
router.post('/emissions',Emission.calculate_item_emission);
router.post('/flight',Emission.calculate_flight_emission);
router.post('/vehicle', Emission.claculate_vechile_emission);
router.post('/trains',Emission.calculate_train_emission);
router.post('/poultry',Emission.calculate_Poultry_emission);
router.post('/appliances',Emission.calculate_appliances_emission);
router.post('/quantity',Emission.calculate_item_quantity);

/*
#######################################################################
|-----------------------More Routes-----------------------------------|
#######################################################################
*/

//router.get('/foos',Foo.dosomething)

module.exports = router;

//curl test- curl -H "Content-Type: application/json" -X POST -d '{"item":"electricity","region":"Africa","unit":"kWh","quantity":1}' http://localhost:3080/v1/emissions
//curl test- curl -H "Content-Type: application/json" -X POST -d '{"item":"airplane model A380","region":"Default","unit":"nm","quantity":125}' http://localhost:3080/v1/emissions
//curl test- curl -H "Content-Type: application/json" -X POST -d '{"type":"Petrol","distance":100,"unit":"km","mileage":50,"mileage_unit":"km/L"}' http://localhost:3080/v1/vehicle
//curl test- curl -H "Content-Type: application/json" -X POST -d '{"type":"international","model":"A380","origin":"DEL","destination":"IXG"}' http://localhost:3080/v1/flight